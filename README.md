# Folio Images

Image repo based on official repository:

https://github.com/folio-org/folio-install/tree/master/alternative-install/kubernetes-rancher/TAMU

## Install Folio via Helm

### 0. Prerequisites

* Helm3
* PostgreSQL server

### 1. Install and configure PostgreSQL

Configure two databases:

* okapi
* okapi modules

### 2. Create folio namespace

`kubectl create ns folio`

### 2. Create database credentials secrets

**db-modules-secret.yaml**:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: db-connect-modules
  namespace: folio
stringData:
  DB_DATABASE: okapi_modules
  DB_PASSWORD: password
  DB_USERNAME: folio_admin
  DB_HOST: pgsql01
  DB_PORT: "5432"
  OKAPI_URL: http://okapi:9130
```

`kubectl apply -f db-modules-secret.yaml`

**db-okapi-secret.yaml**:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: db-connect-okapi
  namespace: folio
stringData:
  DB_DATABASE: okapi
  DB_PASSWORD: password
  DB_USERNAME: folio
  DB_HOST: pgsql01
```

`kubectl apply -f db-okapi-secret.yaml`

### 4. Add folio helm repos

```
helm repo add folio https://svkpk.github.io/folio-helm
helm repo add folio-tasks https://svkpk-public.gitlab.io/folio-images
```

### 5. Install okapi

**okapi-values.yaml**

```yaml
ingress:
  enabled: true
  annotations:
    kubernetes.io/ingress.class: nginx
    kubernetes.io/tls-acme: "true"
    cert-manager.io/cluster-issuer: "letsencrypt-prod"
  hosts:
    - host: okapi.k8s.example.com
      paths:
        - path: /
          pathType: ImplementationSpecific
  tls:
    - secretName: okapi.k8s.example.com
      hosts:
        - okapi.k8s.example.com
postJob:
  enabled: true
  okapiUrl: http://okapi:9130
  tenantId: svkpk
  sampleData: true
  referenceData: true
```

Install okapi via helm

```bash
helm upgrade --install --wait --atomic --timeout 10m \
  --create-namespace \
  --values okapi-values.yaml \
  --namespace=folio okapi folio/okapi
```

### 6. Create tenant

**tenant-values.yaml**

```yaml
image:
  repository: registry.gitlab.com/svkpk-public/folio-images/create-tenant
  tag: latest
jobs:
  createTenant:
    enabled: true
settings:
  tenantId: "svkpk"
  tenantName: "Studijní a vědecká knihovna Plzeňského kraje"
  tenantDescription: "Studijní a vědecká knihovna Plzeňského kraje"
  okapiUrl: "http://okapi:9130"
```

```bash
helm upgrade --install --wait --atomic --timeout 10m \
  --create-namespace   --values tenant-values.yaml \
  --namespace=folio create-tenant folio-tasks/folio-install-jobs
```

### 7. Install users modules

**mods-values.yaml**

```yaml
postJob:
  enabled: true
  okapiUrl: http://okapi:9130
  tenantId: svkpk
  sampleData: false
  referenceData: true
```

```bash
for mod in mod-password-validator mod-authtoken mod-users mod-login mod-permissions mod-inventory-storage mod-users-bl; do
  helm upgrade --install --wait --atomic --timeout 10m \
    --create-namespace   --values mods-values.yaml \
    --namespace=folio $mod folio/$mod
done
```

### 8. Create tenant superuser

**tenant-superuser-values.yaml**

```yaml
image:
  repository: registry.gitlab.com/svkpk-public/folio-images/bootstrap-superuser
  tag: latest
jobs:
  bootstrapSuperuser:
    enabled: true
settings:
  tenantId: "svkpk"
  okapiUrl: "http://okapi:9130"
  adminUser: "svkpk_admin"
  adminPassword: "svkpk_admin_password"
```

```bash
helm upgrade --install --wait --atomic --timeout 10m \
  --create-namespace \
  --values tenant-superuser-values.yaml \
  --namespace=folio create-tenant-superuser folio-tasks/folio-install-jobs
```

### 9. Install stripes

**stripes-values.yaml**

```yaml
image:
  repository: registry.gitlab.com/svkpk-public/folio-images/stripes-svkpk
  tag: latest
ingress:
  enabled: true
  annotations:
    kubernetes.io/ingress.class: nginx
    kubernetes.io/tls-acme: "true"
    cert-manager.io/cluster-issuer: "letsencrypt-prod"
  hosts:
    - host: folio.k8s.example.com
      paths:
        - path: /
          pathType: ImplementationSpecific
  tls:
    - secretName: folio.k8s.example.com
      hosts:
        - folio.k8s.example.com
```

```bash
helm upgrade --install --wait --atomic --timeout 10m \
  --create-namespace \
  --values stripes-values.yaml \
  --namespace=folio stripes folio-tasks/stripes
```
